#ifndef __DRV_ASYN_FILE_H__

#define __DRV_ASYN_FILE_H__

#include <glob.h>

#include <string>

#include <asynPortDriver.h>


class drvAsynFile : public asynPortDriver
{
public:
    enum AddressTypes {List = 0, Filename = 1, Data = 2};
    drvAsynFile(const char *portName, const char *path_to_expose, int max_filesize, bool no_iointr_pattern_match = false);

    asynStatus readOctet(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason);
    asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual);
    asynStatus flushOctet(asynUser *pasynUser);

    asynStatus connect(asynUser *pasynUser);
    asynStatus disconnect(asynUser *pasynUser);

    asynStatus lock();
    asynStatus unlock();

    void       setMaxFilesize(size_t max_filesize);

private:
    class Glob
    {
    public:
        Glob(const char *pattern);
        ~Glob();

        const char* getNextFilename();
        void        reset();

    private:
        glob_t  m_Glob;
        int     m_Index;
    };

    void       createGlob();
    asynStatus callCallbacks();
    asynStatus writePattern(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual);
    asynStatus writeFilename(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual);

    asynStatus readPatternMatch(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason);
    asynStatus readFilename(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason);
    asynStatus readFile(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason);
    asynStatus openFile(asynUser *pasynUser);

    enum ReadTypes {FLUSH, NORMAL, EOS};

    std::string  m_ExposedPath;
    std::string  m_Pattern;
    std::string  m_Filename;
    const char  *m_PatternMatchPointer;
    const char  *m_FilenameReadPointer;
    ReadTypes    m_FilenameRead;
    ReadTypes    m_PatternMatchRead;
    bool         m_InSession;
    Glob        *m_Glob;
    bool         m_IOIntrPatternMatch;
    size_t       m_MaxFilesize;
    size_t       m_NumBytesRead;
};


#endif	/* __DRV_ASYN_FILE_H__ */
